module.exports = function(grunt) {
  'use strict';

  //load tasks
  require('load-grunt-tasks')(grunt);

  var configBridge = grunt.file.readJSON('node_modules/bootstrap/grunt/configBridge.json', { encoding: 'utf8' });

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*!\n' +
            ' * <%= pkg.themeConfig.name %> Theme v<%= pkg.version %> <%= pkg.themeConfig.URI %>\n' +
            ' * Copyright <%= grunt.template.today("yyyy") %> <%= pkg.author %>\n' +
            ' */\n',
    jqueryCheck: configBridge.config.jqueryCheck.join('\n'),
    jqueryVersionCheck: configBridge.config.jqueryVersionCheck.join('\n'),
    themeBanner:'/*\n' +
            ' Theme Name: <%= pkg.themeConfig.name %>\n'+
            ' Theme URI: <%= pkg.themeConfig.URI %>\n'+
            ' Author: <%= pkg.author %>\n'+
            ' Author URI: <%= pkg.themeConfig.authorURI %>\n'+
            ' Description: <%= pkg.themeConfig.description %>\n'+
            ' Version: <%= pkg.version %>\n'+
            ' License: GNU General Public License v2 or later\n'+
            ' License URI: http://www.gnu.org/licenses/gpl-2.0.html\n'+
            ' Text Domain: <%= pkg.themeConfig.textDomain %>\n'+
            ' Tags: <%= pkg.themeConfig.tags %>\n'+
            ' */\n',
    
    /**
     * Copy some files from dev source
     */
    copy: {
      tgma: {
        expand: true,
        cwd: '<%= pkg.composerDir %>',
        src: '*.php',
        dest: '<%= pkg.themeDir %>/includes/'
      },
      jshint: {
        expand: true,
        cwd: '<%= pkg.bootstrapDir %>',
        src: '**/.jshintrc',
        dest: ''
      },
      jsfrone: {
        expand: true,
        cwd: 'js/',
        src: 'jquery.frone.js',
        dest: '<%= pkg.themeJS %>'
      },
      jscookie: {
        expand: true,
        cwd: '<%= pkg.cookieDir %>',
        src: 'js.cookie.js',
        dest: '<%= pkg.frameworkDir %>js/'
      },
      fonts: {
        files: [
          { expand: true, cwd: '<%= pkg.fontAwesomeDir %>fonts/', src: '*.*', dest: '<%= pkg.themeDir %>fonts/' },
          { expand: true, cwd: '<%= pkg.fontAwesomeDir %>css/', src: '*.*', dest: '<%= pkg.themeCSS %>' },
          { expand: true, cwd: '<%= pkg.materialIconsDir %>', src: 'MaterialIcons-Regular.*', dest: '<%= pkg.themeDir %>fonts/' }
        ]
      },
      select2: {
        files: [
          { expand: true, cwd: '<%= pkg.select2Dir %>js/', src: 'select2.min.js', dest: '<%= pkg.frameworkDir %>js/' },
          { expand: true, cwd: '<%= pkg.select2Dir %>css/', src: 'select2.min.css', dest: '<%= pkg.frameworkDir %>css/' }
        ]
      }
    },

    /**
     * Check JS files
     *
     */
    jshint: {
      options: {
        jshintrc: 'js/.jshintrc'
      },
      grunt: {
        options: {
          jshintrc: 'grunt/.jshintrc'
        },
        src: ['Gruntfile.js', 'grunt/*.js']
      },
      compileCore: {
        src: 'js/*.js'
      },
      checkFramework: {
        src: '<%= pkg.frameworkDir %>js/ts-framework.js'
      }
    },

    /**
     * Concat JS files
     *
     */
    concat: {
      options: {
        banner: '<%= banner %>\n<%= jqueryCheck %>\n<%= jqueryVersionCheck %>',
        stripBanners: false
      },
      plugins: {
        src: [
          '<%= pkg.bootstrapDir %>js/transition.js',
          //'js/alert.js',
          //'js/button.js',
          //'js/carousel.js',
          //'js/collapse.js',
          //'js/dropdown.js',
          '<%= pkg.bootstrapDir %>js/modal.js',
          '<%= pkg.bootstrapDir %>js/tooltip.js',
          //'js/popover.js',
          //'js/scrollspy.js',
          //'js/tab.js',
          //'js/affix.js',
          '<%= pkg.wavesDir %>dist/waves.js',
          //'js/plugins/jquery.waitforimages.js',
          //'js/plugins/jquery.waypoints.js',
          //'js/plugins/perfect-scrollbar.jquery.js'
        ],
        dest: '<%= pkg.themeJS %>jquery.plugins.js'
      }
    },

    /**
     * Minify JS files
     *
     */
    uglify: {
      options: {
        preserveComments: 'some'
      },
      compileCore: {
        src: '<%= pkg.themeJS %>jquery.frone.js',
        dest: '<%= pkg.themeJS %>jquery.frone.min.js'
      },
      compilePlugins: {
        src: '<%= pkg.themeJS %>jquery.plugins.js',
        dest: '<%= pkg.themeJS %>jquery.plugins.min.js'
      },
      compileFramework: {
        files: {
          '<%= pkg.frameworkDir %>js/js.cookie.min.js': ['<%= pkg.frameworkDir %>js/js.cookie.js'],
          '<%= pkg.frameworkDir %>js/ts-framework.min.js': ['<%= pkg.frameworkDir %>js/ts-framework.js']
        }
      }
    },

    /**
     * Use this banner
     *
     */
    usebanner: {
      options: {
        position: 'top'
      },
      themeStyle: {
        options: {
          banner: '<%= themeBanner %>'
        },
        src: '<%= pkg.themeDir %>style.css'
      },
      themeCSS: {
        options: {
          banner: '<%= banner %>'
        },
        src: ['<%= pkg.themeCSS %>frone.css', '<%= pkg.themeCSS %>frone.min.css']
      },
      rtlCSS: {
        options: {
          banner: '<%= banner %>'
        },
        src: ['<%= pkg.themeCSS %>rtl.css', '<%= pkg.themeCSS %>rtl.min.css']
      },
      iconsCSS: {
        options: {
          banner: '<%= banner %>'
        },
        src: ['<%= pkg.themeCSS %>icons.css', '<%= pkg.themeCSS %>icons.min.css']
      },
      editorCSS: {
        options: {
          banner: '<%= banner %>'
        },
        src: ['<%= pkg.themeCSS %>editor.css', '<%= pkg.themeCSS %>editor.min.css']
      },
      pluginJS: {
        options: {
          banner: '<%= banner %>'
        },
        src: '<%= pkg.themeJS %>jquery.plugins.min.js'
      },
      themeJS: {
        options: {
          banner: '<%= banner %>'
        },
        src: ['<%= pkg.themeJS %>jquery.frone.js', '<%= pkg.themeJS %>jquery.frone.min.js']
      }
    },

    /**
     * Minify CSS files
     *
     */
    cssnano: {
      options: {
        sourcemap: false,
        discardComments: true,
        reduceIdents: false,
        zindex: false
      },
      dist: {
        files: {
          '<%= pkg.themeCSS %>frone.min.css': ['<%= pkg.themeCSS %>frone.css'],
        }
      },
      rtl: {
        files: {
          '<%= pkg.themeCSS %>rtl.min.css': ['<%= pkg.themeCSS %>rtl.css'],
        }
      },
      icons: {
        files: {
          '<%= pkg.themeCSS %>icons.min.css': ['<%= pkg.themeCSS %>icons.css'],
        }
      },
      editor: {
        files: {
          '<%= pkg.themeCSS %>editor.min.css': ['<%= pkg.themeCSS %>editor.css'],
        }
      },
      style: {
        files: {
          '<%= pkg.themeDir %>style.css': ['<%= pkg.themeDir %>style.css'],
        }
      }
    },

    /**
     * Apply several post-processors to your CSS using PostCSS.
     *
     */
    postcss: {
      options: {
        map: false, // inline sourcemaps
        processors: [
          require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({cascade: false, browsers: 'last 2 versions'}), // add vendor prefixes
          //require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: '<%= pkg.themeCSS %>/*.css'
      }
    },

    /**
     * Sort CSS Lines
     *
     */
    csscomb: {
      options: {
        config: 'less/.csscomb.json'
      },
      dist: {
        files: {
          '<%= pkg.themeCSS %>frone.css': ['<%= pkg.themeCSS %>frone.css'],
        }
      },
      rtl: {
        files: {
          '<%= pkg.themeCSS %>rtl.css': ['<%= pkg.themeCSS %>rtl.css'],
        }
      },
      editor: {
        files: {
          '<%= pkg.themeCSS %>editor.css': ['<%= pkg.themeCSS %>editor.css'],
        }
      }
    },

    /**
     * Compile LESS files
     *
     */
    less: {
      compileCore: {
        options: {
          strictMath: true
        },
        src: 'less/frone.less',
        dest: '<%= pkg.themeCSS %>frone.css'
      },
      compileRTL: {
        options: {
          strictMath: true
        },
        src: 'less/rtl.less',
        dest: '<%= pkg.themeCSS %>rtl.css'
      },
      compileIcons: {
        options: {
          strictMath: true
        },
        src: 'less/icons.less',
        dest: '<%= pkg.themeCSS %>icons.css'
      },
      compileEditor: {
        options: {
          strictMath: true
        },
        src: 'less/editor.less',
        dest: '<%= pkg.themeCSS %>editor.css'
      }
    },

    /**
     * Watch actions
     *
     */
    watch: {
      less: {
        files: ['less/**/*.less', '!less/rtl.less', '!less/editor.less', '!less/icons.less'],
        tasks: ['less:compileCore', 'csscomb:dist', 'postcss', 'cssnano:dist', 'usebanner:themeCSS'],
        options: {
          livereload: true
        }
      },
      rtl: {
        files: 'less/rtl.less',
        tasks: ['less:compileRTL', 'csscomb:rtl', 'postcss', 'cssnano:rtl', 'usebanner:rtlCSS'],
        options: {
          livereload: true
        }
      },
      icons: {
        files: 'less/icons.less',
        tasks: ['less:compileIcons', 'cssnano:icons']
      },
      editor: {
        files: 'less/editor.less',
        tasks: ['less:compileEditor', 'csscomb:editor', 'postcss', 'cssnano:editor', 'usebanner:editorCSS']
      },
      js: {
        files: 'js/*.js',
        tasks: ['jshint:compileCore', 'copy:jsfrone', 'uglify:compileCore', 'usebanner:themeJS'],
        options: {
          livereload: true
        }
      },
      fw: {
        files: '<%= pkg.frameworkDir %>js/ts-framework.js',
        tasks: ['jshint:checkFramework', 'uglify:compileFramework']
      },
      php: {
        files: ['<%= pkg.themeDir%>/**/*.php'],
        options: {
          livereload: true
        }
      }
    }
    
  });

  // Load the plugins that provides the tasks.
  //grunt.loadNpmTasks('grunt-contrib-copy');
  
  // Default task(s).
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('frone-plugins', ['concat:plugins', 'uglify:compilePlugins', 'usebanner:pluginJS']);
  grunt.registerTask('frone-dist', ['cssnano:style', 'usebanner:themeStyle']);

};