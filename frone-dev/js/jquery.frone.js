/*global Waves */
;(function ( $, window, document, undefined ) {
  'use strict';

  var $window = $(window),
      $document = $(document),
      $body     = $('body'),
      window_height = $window.height(),
      is_screen,
      is_device_mobile = (navigator.userAgent.match(/(Android|iPhone|iPad|iPod|Opera Mini|webOS|BlackBerry|IEMobile)/)) ? true : false;


  /**
   * Exists Element
   *
   * @since 1.0
   */
  $.exists = function(selector) {
    return ($(selector).length > 0);
  };

  /**
   * Viewport Bootstrap
   *
   * @since 1.0
   */
  $.viewportBS = function() {
    if( $window.width() < 768 ) {
      is_screen = 'xs';
    }else if ( $window.width() >= 768 && $window.width() <= 992 ) {
      is_screen = 'sm';
    }else if( $window.width() > 992 && $window.width() <= 1200 ) {
      is_screen = 'md';
    }else  {
      is_screen = 'lg';
    }
  }

  /**
   * Mobile Menu
   *
   */
  $.fn.froneMobileMenu = function() {

    return this.each(function() {

      var _this   = $(this),
          _menu_btn = _this.find('.frone-mobile-menu-btn'),
          _close_btn = _this.find('.frone-mobile-nav-bg'),
          _main   = _this.find('.menu-item-has-children > a[href="#"]'),
          _sub    = _this.find('.sub-menu');

      _sub.before('<span class="md-icon mobile-sub-menu-btn">arrow_drop_down</span>');

      $('.mobile-sub-menu-btn').click(function() {
        $(this).next().slideToggle();
      });

      _main.click(function(e) {
        e.preventDefault();
        $(this).next().next().slideToggle();
      });

      _menu_btn.click(function(e) {
        e.preventDefault();
        $('body').addClass('mobile-menu-active').removeClass('mobile-menu-passive');
      });

      _close_btn.click(function() {
        $('body').removeClass('mobile-menu-active').addClass('mobile-menu-passive');
      });

    });
  };

  /**
   * Site Header
   *
   * @since 1.0
   */
  $.fn.froneSiteHeader = function() {

    return this.each(function() {
      var $this = $(this),
          _header_outher = $this.outerHeight(),
          _header_height = _header_outher * -1.2,
          _top_position = 0,
          _previous_scroll = 0,
          _current_scroll;

      //
      function _sticky_header(){
        //Get current scroll position
        _current_scroll = $window.scrollTop();

        //Check scroll position
        if( _current_scroll === 0 ) {
          _top_position = $('body').hasClass('admin-bar') ? $('#wpadminbar').height() : 0;
          $this.css('top', _top_position).removeClass('sticky-header');
        }else if( ( _current_scroll > _previous_scroll || $window.scrollTop() + window_height == $document.height() ) && !$this.hasClass('sticky-header') ) {
          $this.css('top', _header_height);
        }else if( _current_scroll < _previous_scroll && !$this.hasClass('sticky-header') ) {
          $this.addClass('sticky-header');
        }else if( _current_scroll > _previous_scroll && $this.hasClass('sticky-header') ) {
          $this.css('top', _header_height).removeClass('sticky-header');
        }

        //Get latest position
        _previous_scroll = _current_scroll;
      }

      //
      $window.bind('scroll', _sticky_header).bind('resize', _sticky_header);

      //header space
      if( !$('#page-header').exists ) {
        $body.css('margin-top', _header_outher);
      }

    });
  };



  /**
   * Hooks
   *
   * @since 1.0
   */
  $(document).ready( function(){
    //
    $('#header').froneSiteHeader();

    $('.frone-mobile-sidemenu').froneMobileMenu();

    //
    if(is_device_mobile) {
      $('body').addClass('touchable-device');
    }

    //
    if( typeof Waves === 'object' ) {
      Waves.attach('.mobile-sub-menu-btn');
      Waves.attach('.frone-mobile-nav a');
      Waves.attach('.frone-mobile-menu-btn');
      Waves.init();
    }

    //Get Viewport
    $.viewportBS();
  });

  $(window).resize( function(){
    window_height = $window.height();
    $.viewportBS();
  });

  $(window).scroll( function(){
    // do stuff
  });

  $(window).load( function(){
    // do stuff
  });

})( jQuery, window, document );