<?php
/*
Plugin Name: Frone Plugin Dev
Plugin URI: https://themesama.com/
Description: 
Version: 1.0
Author: Themesama
Author URI: https://themesama.com
License: GPLv2 or later
Text Domain: ts2017dev
*/

add_filter( 'https_ssl_verify', '__return_false' );

function frone_dev_enqueue_scripts() {
  //Live reload
  wp_enqueue_script( 'reload-frone', '//localhost:35729/livereload.js' );
}

add_action( 'wp_enqueue_scripts', 'frone_dev_enqueue_scripts' );