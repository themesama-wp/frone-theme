<?php
/**
 * The template for displaying Category pages
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <h1><?php printf( __( 'Category Archives: %s', 'frone' ), single_cat_title( '', false ) ); ?></h1>
  
  <?php
    // Show an optional term description.
    $term_description = term_description();
    if ( ! empty( $term_description ) ){
      printf( '<p>%s</p>', $term_description );
    }
  ?>

  <?php
    /**
     * 20 - frone_loop
     *
     */
    do_action( 'frone_category' ); ?>

<?php get_footer(); ?>