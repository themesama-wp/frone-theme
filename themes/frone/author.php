<?php
/**
 * The template for displaying Author archive pages
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <h1><?php printf( __( 'All posts by %s', 'frone' ), get_the_author() ); ?></h1>
  
  <?php 
    if ( get_the_author_meta( 'description' ) ) {
      echo '<p><a href="#">'.get_avatar( get_the_author_meta('ID'), 120 ).'</a>'.get_the_author_meta( 'description' ).'</p>';
    }
  ?>

  <?php
    /**
     * 20 - frone_loop
     *
     */
    do_action( 'frone_author' ); ?>

<?php get_footer(); ?>