<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <h1><?php _e( 'Oops! That page can&rsquo;t be found.', 'frone' ); ?></h1>
  <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'frone' ); ?></p>

  <?php get_search_form(); ?>

<?php get_footer(); ?>