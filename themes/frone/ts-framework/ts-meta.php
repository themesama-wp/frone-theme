<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Themesama Framework Meta
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Meta') ) {

class TS_Framework_Meta extends TS_Framework {

  public $theme_meta_tabs = array();
  
  /**
   * Constructor
   *
   * @since 1.0.1
   */
  function __construct() {
    //all customization options
    $this->theme_meta_tabs = apply_filters( 'ts_framework_meta_tabs', array() );

    $this->add_action( 'admin_init', 'admin_init' );
    $this->add_action( 'save_post', 'save_meta_boxes' );
  }

  /**
   * Admin Init
   *
   * @since 1.0.1
   */
  public function admin_init() {
    $this->add_action( 'add_meta_boxes', 'add_meta_boxes' );
  }

  /**
   * Add Meta Boxes
   *
   * @since 1.0.1
   */
  public function add_meta_boxes( $page ) {

    foreach ($this->theme_meta_tabs as $key => $metabox) {
      //control
      if( empty( $metabox['id'] ) || empty( $metabox['label'] ) || empty( $metabox['post_type'] ) || empty( $metabox['context'] ) || empty( $metabox['priority'] ) || empty( $metabox['tabs'] ) ) {
        continue;
      }

      if( is_array( $metabox['post_type'] ) ) {
        //add multible metabox
        foreach ($metabox['post_type'] as $metabox_multible) {
          add_meta_box( $metabox['id'], $metabox['label'], array( $this, 'meta_box_content' ), $metabox_multible, $metabox['context'], $metabox['priority'], $metabox['tabs'] );
        }
      }else {
        //add meta box
        add_meta_box( $metabox['id'], $metabox['label'], array( $this, 'meta_box_content' ), $metabox['post_type'], $metabox['context'], $metabox['priority'], $metabox['tabs'] );
      }
    }

  }

  /**
   * Meta Box Content & Tabs
   *
   * @since 1.0.1
   */
  public function meta_box_content( $post, $options ) {
    //Current Meta Values
    $current_meta_values = get_post_meta( $post->ID, $options['id'], true );

    //first security
    wp_nonce_field( 'ts_custom_meta_boxes', 'ts_custom_meta_boxes_nonce' );

    $all_tabs = '<ul class="ts-meta-tabs">';
    $all_controls = '<div class="ts-meta-tabs-content '.( count( $options['args'] ) == 1 ? 'ts-meta-tab-single' : '' ).'">';

    foreach ($options['args'] as $tab_id => $tab) {
      //control
      if( empty( $tab['id'] ) || empty( $tab['label'] ) || empty( $tab['controls'] ) ) {
        continue;
      }

      $all_tabs .= '<li class="ts-meta-tab-link" data-metatab-id="'.esc_attr( $tab['id'] ).'"><a href="#">'.esc_html( $tab['label'] ).'</a></li>';
      $all_controls .= '<div class="ts-meta-tab" data-metatab-id="'.esc_attr( $tab['id'] ).'">';

      foreach ($tab['controls'] as $control_id => $control) {
        //control
        if( empty( $control['label'] ) || empty( $control['id'] ) || empty( $control['type'] ) ) {
          continue;
        }

        $control['name'] = $options['id'].'['.$control['id'].']';
        //current value
        if( isset( $current_meta_values[ $control['id'] ] ) ) {
          $control['value'] = $current_meta_values[ $control['id'] ];
        }

        $all_controls .= $this->get_field( $control );
      }

      $all_controls .= '</div>';

    }

    $all_tabs .= '</ul>';
    $all_controls .= '</div>';

    echo '<div class="ts-meta-boxes" data-id="'.esc_attr( $options['id'] ).'">'.( count( $options['args'] ) > 1 ? $all_tabs : '' ).$all_controls.'</div>';
  }

  /**
   * Save Meta Boxes
   *
   * @since 1.0.1
   */
  public function save_meta_boxes( $post_id ) {
    //check fields & permissions
    $nonce = isset($_POST['ts_custom_meta_boxes_nonce']) ? $_POST['ts_custom_meta_boxes_nonce'] : '';

    if ( ( isset($_POST['post_type']) && 'page' != $_POST['post_type'] && !current_user_can( 'edit_post', $post_id ) ) || ( isset($_POST['post_type']) &&'page' == $_POST['post_type'] && !current_user_can( 'edit_page', $post_id ) ) || ! wp_verify_nonce( $nonce, 'ts_custom_meta_boxes' ) || ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) ){
      return $post_id;
    }

    do_action( 'ts_metabox_save_before', $post_id );

    /* OK, its safe for us to save the data now. */
    foreach ($this->theme_meta_tabs as $metabox_id => $metabox) {
      //check
      if( !isset( $_POST[$metabox['id']] ) ) {
        continue;
      }

      //values
      $metabox_values = array();

      foreach ($metabox['tabs'] as $tab_id => $tab) {
        foreach ($tab['controls'] as $control_id => $control) {
          //sanitize field
          $metabox_field_value = isset( $_POST[$metabox['id']][ $control['id'] ] ) ? $_POST[$metabox['id']][ $control['id'] ] : '';
          $metabox_values[ $control['id'] ] = $this->sanitize_field( $control['type'], $metabox_field_value, $control );
        }
      }
      
      //check array
      if ( !empty( $metabox_values ) && is_array( $metabox_values ) ) {
        update_post_meta( $post_id, $metabox['id'], $metabox_values );
        unset( $metabox_values );
      }

    }

  }

}

}

new TS_Framework_Meta();