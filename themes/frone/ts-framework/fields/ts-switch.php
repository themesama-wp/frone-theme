<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Switch Field
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Switch_Field') && class_exists('TS_Framework_Field') ) {

class TS_Framework_Switch_Field extends TS_Framework_Field {

  public $type = 'checkbox';

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render(){
    $this->add_attr( 'type', 'checkbox' );
    $this->add_attr( 'id', $this->field['id'] );

    $this->field_output .= '<div class="ts-switch">
      <input '.$this->field_attrs.( !empty( $this->field['value'] ) ? checked( '1', $this->field['value'], false ) : '' ).'>
      <label for="'.esc_attr( $this->field['id'] ).'"></label>
    </div>';

  }

}

}