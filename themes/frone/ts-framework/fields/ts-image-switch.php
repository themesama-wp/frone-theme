<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Image Switch Field
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Image_Switch_Field') && class_exists('TS_Framework_Field') ) {

class TS_Framework_Image_Switch_Field extends TS_Framework_Field {

  public $type = 'radio';

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render(){
    $this->add_attr( 'type', 'radio' );

    $i = 0;

    if( !empty( $this->field['choices'] ) && is_array( $this->field['choices'] ) ) {
      foreach ($this->field['choices'] as $option_value => $option) {
        $i++;
        //output
        $this->field_output .= '<div class="ts-image-switch">';

        $this->field_output .= '<input id="'.esc_attr( $this->field['id'].'_'.$i ).'" value="'.esc_attr( $option_value ).'" '.$this->field_attrs.( !empty( $this->field['value'] ) ? checked( $option_value, $this->field['value'], false ) : '' ).'> ';
        $this->field_output .= '<label for="'.esc_attr( $this->field['id'].'_'.$i ).'"><img src="'.esc_url( $option ).'" alt=""></label>';

        $this->field_output .= '</div>';
      }
    }

  }

}

}