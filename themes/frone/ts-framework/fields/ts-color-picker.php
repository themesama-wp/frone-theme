<?php
if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Color Picker Field
 *
 * @since 1.0.1
 */
if( !class_exists('TS_Framework_Color_Field') && class_exists('TS_Framework_Field') ) {

class TS_Framework_Color_Field extends TS_Framework_Field {
  
  public $type = 'text';

  /**
   * Render Field
   *
   * @since 1.0.1
   */
  public function render(){
    //field extra attrs
    $this->add_attr( 'type', 'text' );
    $this->add_attr( 'maxlength', '7' );
    if( !empty( $this->field['value'] ) ) {
      $this->add_attr( 'value', $this->field['value'] );
    }
    //output
    $this->field_output .= '<div class="ts-form-field type-color"><input '.$this->field_attrs.'></div>';

  }

}

}