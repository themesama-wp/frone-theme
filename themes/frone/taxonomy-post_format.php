<?php
/**
 * The template for displaying Tag pages
 *
 * @author Themesama
 * @since 1.0
 */
 
get_header(); ?>

  <?php
    /**
     * 15 - frone_taxonomy_post_format_title
     * 20 - frone_loop
     *
     */
    do_action( 'frone_taxonomy_post_format' ); ?>
      
<?php get_footer(); ?>