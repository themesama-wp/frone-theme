<?php
/**
 * The template for displaying archive pages
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <?php 
    if ( have_posts() ) {
      the_archive_title( '<h1>', '</h1>' );
      the_archive_description( '<p>', '</p>' );
    }
  ?>

  <?php
    /**
     * 20 - frone_loop
     *
     */
    do_action( 'frone_archive' ); ?>

<?php get_footer(); ?>