<?php
  if ( have_posts() ) {
    /* Start the Loop */
    while ( have_posts() ) { 
      //
      the_post();
      get_template_part( 'template-parts/post/content', get_post_format() );
    }

    the_posts_pagination( array(
      'prev_text' => __( 'Previous page', 'frone' ),
      'next_text' => __( 'Next page', 'frone' ),
      'before_page_number' => __( 'Page', 'frone' ),
    ) );

  } else {
    get_template_part( 'template-parts/post/content', 'none' );
  }

  get_sidebar();
?>