<?php
/**
 * Template part for displaying page content in page.php
 *
 * @author Themesama
 * @since 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <?php
    the_content();

    wp_link_pages( array(
      'before' => '<div class="page-links">' . __( 'Pages:', 'frone' ),
      'after'  => '</div>'
    ) );
  ?>
</article><!-- #post-<?php the_ID(); ?> -->