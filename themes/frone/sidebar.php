<?php
/**
 * The sidebar containing the main widget area
 *
 * @author Themesama
 * @since 1.0
 */

?>
<?php
$frone_sidebar = frone_theme_mod('blog', 'sidebar');

if( $frone_sidebar ) {
?>
<aside role="complementary">
  <?php dynamic_sidebar( 'frone-sidebar-main' ); ?>
</aside>
<?php
}