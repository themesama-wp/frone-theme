<?php
/**
 * The template for displaying the header
 *
 * @author Themesama
 * @since 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <script>(function(){document.documentElement.className='js'})();</script>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php
  /**
   *
   */
  do_action( 'frone_header_before' ); ?>

  <header id="header">
    <div class="<?php echo apply_filters( 'frone_container', 'container-fluid' ); ?>">
      <div class="row">
        <div class="col-xs-12">

        <?php 
        /**
         *
         */
        do_action( 'frone_header' ); ?>

        </div>
      </div>
    </div>
  </header><!-- #header -->

  <?php
  /**
   *
   */
  do_action( 'frone_header_after' ); ?>