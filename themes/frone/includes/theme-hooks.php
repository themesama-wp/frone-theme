<?php
/**
 * Theme Hooks (Actions and Filters)
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 * 
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Theme Style & Scripts (theme-style.php)
 *
 */
add_action( 'wp_enqueue_scripts', 'frone_enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'frone_custom_inline_styles' );
add_action( 'admin_init', 'frone_add_editor_styles' );
add_action( 'body_class', 'frone_body_class' );

/**
 * Theme Functions (theme-functions.php)
 *
 * Theme Support
 * Widgets Init
 */
add_action( 'after_setup_theme', 'frone_theme_support' );
add_action( 'widgets_init', 'frone_widgets_init' );

//Required plugins
add_action( 'tgmpa_register', 'frone_register_required_plugins' );

/**
 * Header
 *
 */
add_action( 'frone_header', 'frone_mobile_menu', 15 );
add_action( 'frone_header', 'frone_logo', 20 );
add_action( 'frone_header', 'frone_main_menu', 25 );
add_action( 'frone_header', 'frone_extra_menu', 30 );

/**
 * Archive Page Actions
 *
 */
add_action( 'frone_archive', 'frone_loop', 20 );

/**
 * Author Page Actions
 *
 */
add_action( 'frone_author', 'frone_loop', 20 );

/**
 * Catogory Actions
 *
 */
add_action( 'frone_category', 'frone_loop', 20 );

/**
 * Index Actions
 *
 */
add_action( 'frone_index', 'frone_loop', 20 );

/**
 * Tag Actions
 *
 */
add_action( 'frone_tag', 'frone_loop', 20 );

/**
 * Taxonomy Post Format Actions
 *
 */
add_action( 'frone_taxonomy_post_format', 'frone_taxonomy_post_format_title', 15 );
add_action( 'frone_taxonomy_post_format', 'frone_loop', 20 );