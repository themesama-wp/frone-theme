<?php
/**
 * TS Framework Customization Filter
 *
 * Control Types: switch, image-switch, color, upload, image, text, checkbox, textarea, dropdown-pages, radio, select
 * Radio and Select controls(requires choices array in $args)
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Font Weight
 *
 */
if( !function_exists( 'ts_framework_fonts_weight' ) ) {
function ts_framework_fonts_weight() {
  return apply_filters( 'frone_fonts_weight', array(
    'normal'  => 'normal',
    'bold'    => 'bold',
    'bolder'  => 'bolder',
    'lighter' => 'lighter',
    'initial' => 'initial',
    'inherit' => 'inherit',
    '100'     => '100',
    '200'     => '200',
    '300'     => '300',
    '400'     => '400',
    '500'     => '500',
    '600'     => '600',
    '700'     => '700',
    '800'     => '800',
    '900'     => '900'
  ) );
}
}

/**
 * Font Pair List
 *
 */
if( !function_exists( 'ts_framework_font_pair' ) ) {
function ts_framework_font_pair() {
  return apply_filters( 'frone_font_pair', array(
    'Raleway, sans-serif|Lusitana, serif' => 'Raleway & Lusitana',
    'Source Sans Pro, sans-serif|Times New Roman, serif' => 'Source Sans Pro & Times New Roman',
    'Roboto Slab, serif|Roboto, sans-serif' => 'Roboto Slab & Roboto',
    'Roboto Slab, serif|Open Sans, sans-serif' => 'Roboto Slab & Open Sans',
    'PT Serif, serif|Open Sans, sans-serif' => 'PT Serif & Open Sans',
    'Quattrocento, serif|Quattrocento Sans, sans-serif' => 'Quattrocento & Quattrocento Sans',
    'Dosis, sans-serif|Open Sans, sans-serif' => 'Dosis & Open Sans',
    'Droid Sans, sans-serif|Cabin, sans-serif' => 'Droid Sans & Cabin',
    'Nunito, sans-serif|Open Sans, sans-serif' => 'Nunito & Open Sans',
    'Oxygen, sans-serif|Source Sans Pro, sans-serif' => 'Oxygen & Source Sans Pro',
    'Philosopher, sans-serif|Muli, sans-serif' => 'Philosopher & Muli',
    'Signika, sans-serif|Open Sans, sans-serif' => 'Signika & Open Sans',
    'Ubuntu, sans-serif|Source Sans Pro, sans-serif' => 'Ubuntu & Source Sans Pro',
    'Lobster, cursive|Arimo, sans-serif' => 'Lobster & Arimo',
    'Lobster, cursive|Cabin, sans-serif' => 'Lobster & Cabin',
    'Quattrocento, serif|Fanwood Text, serif' => 'Quattrocento & Fanwood Text'
  ) );

}
}

/**
 * Font List
 *
 */
if( !function_exists( 'ts_framework_google_fonts' ) ) {
function ts_framework_google_fonts() {
  return apply_filters( 'frone_google_fonts', array(
    '' => __('Use font pair', 'frone'),
    'Roboto, sans-serif' => 'Roboto',
    'Open Sans, sans-serif' => 'Open Sans',
    'Lato, sans-serif' => 'Lato',
    'Slabo 27px, serif' => 'Slabo 27px',
    'Oswald, sans-serif' => 'Oswald',
    'Roboto Condensed, sans-serif' => 'Roboto Condensed',
    'Source Sans Pro, sans-serif' => 'Source Sans Pro',
    'Montserrat, sans-serif' => 'Montserrat',
    'Raleway, sans-serif' => 'Raleway',
    'PT Sans, sans-serif' => 'PT Sans',
    'Roboto Slab, serif' => 'Roboto Slab',
    'Merriweather, serif' => 'Merriweather',
    'Open Sans Condensed, sans-serif' => 'Open Sans Condensed',
    'Droid Sans, sans-serif' => 'Droid Sans',
    'Lora, serif' => 'Lora',
    'Ubuntu, sans-serif' => 'Ubuntu',
    'Droid Serif, serif' => 'Droid Serif',
    'Playfair Display, serif' => 'Playfair Display',
    'PT Serif, serif' => 'PT Serif',
    'Arimo, sans-serif' => 'Arimo',
    'Noto Sans, sans-serif' => 'Noto Sans',
    'PT Sans Narrow, sans-serif' => 'PT Sans Narrow',
    'Titillium Web, sans-serif' => 'Titillium Web',
    'Muli, sans-serif' => 'Muli',
    'Poppins, sans-serif' => 'Poppins',
    'Indie Flower, cursive' => 'Indie Flower',
    'Bitter, serif' => 'Bitter',
    'Inconsolata, monospace' => 'Inconsolata',
    'Dosis, sans-serif' => 'Dosis',
    'Fjalla One, sans-serif' => 'Fjalla One',
    'Hind, sans-serif' => 'Hind',
    'Oxygen, sans-serif' => 'Oxygen',
    'Cabin, sans-serif' => 'Cabin',
    'Noto Serif, serif' => 'Noto Serif',
    'Anton, sans-serif' => 'Anton',
    'Arvo, serif' => 'Arvo',
    'Catamaran, sans-serif' => 'Catamaran',
    'Crimson Text, serif' => 'Crimson Text',
    'Lobster, cursive' => 'Lobster',
    'Yanone Kaffeesatz, sans-serif' => 'Yanone Kaffeesatz',
    'Libre Baskerville, serif' => 'Libre Baskerville',
    'Nunito, sans-serif' => 'Nunito',
    'Fira Sans, sans-serif' => 'Fira Sans',
    'Merriweather Sans, sans-serif' => 'Merriweather Sans',
    'Josefin Sans, sans-serif' => 'Josefin Sans',
    'Bree Serif, serif' => 'Bree Serif',
    'Abril Fatface, cursive' => 'Abril Fatface',
    'Exo 2, sans-serif' => 'Exo 2',
    'Ubuntu Condensed, sans-serif' => 'Ubuntu Condensed',
    'Gloria Hallelujah, cursive' => 'Gloria Hallelujah',
    'Abel, sans-serif' => 'Abel',
    'Asap, sans-serif' => 'Asap',
    'Pacifico, cursive' => 'Pacifico',
    'Varela Round, sans-serif' => 'Varela Round',
    'Amatic SC, cursive' => 'Amatic SC',
    'Quicksand, sans-serif' => 'Quicksand',
    'Karla, sans-serif' => 'Karla',
    'Rubik, sans-serif' => 'Rubik',
    'Signika, sans-serif' => 'Signika',
    'Archivo Narrow, sans-serif' => 'Archivo Narrow',
    'Dancing Script, cursive' => 'Dancing Script',
    //Websafe fonts
    'Cambria, &quot;Hoefler Text&quot;, Utopia, &quot;Liberation Serif&quot;, &quot;Nimbus Roman No9 L Regular&quot;, Times, &quot;Times New Roman&quot;, serif' => 'Times New Roman',
    'Constantia, &quot;Lucida Bright&quot;, Lucidabright, &quot;Lucida Serif&quot;, Lucida, &quot;DejaVu Serif&quot;, &quot;Bitstream Vera Serif&quot;, &quot;Liberation Serif&quot;, Georgia, serif' => 'Georgia',
    '&quot;Palatino Linotype&quot;, Palatino, Palladio, &quot;URW Palladio L&quot;, &quot;Book Antiqua&quot;, Baskerville, &quot;Bookman Old Style&quot;, &quot;Bitstream Charter&quot;, &quot;Nimbus Roman No9 L&quot;, Garamond, &quot;Apple Garamond&quot;, &quot;ITC Garamond Narrow&quot;, &quot;New Century Schoolbook&quot;, &quot;Century Schoolbook&quot;, &quot;Century Schoolbook L&quot;, Georgia, serif' => 'Garamond',
    'Frutiger, &quot;Frutiger Linotype&quot;, Univers, Calibri, &quot;Gill Sans&quot;, &quot;Gill Sans MT&quot;, &quot;Myriad Pro&quot;, Myriad, &quot;DejaVu Sans Condensed&quot;, &quot;Liberation Sans&quot;, &quot;Nimbus Sans L&quot;, Tahoma, Geneva, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif' => 'Helvetica/Arial',
    'Corbel, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, &quot;DejaVu Sans&quot;, &quot;Bitstream Vera Sans&quot;, &quot;Liberation Sans&quot;, Verdana, &quot;Verdana Ref&quot;, sans-serif' => 'Verdana',
    '&quot;Segoe UI&quot;, Candara, &quot;Bitstream Vera Sans&quot;, &quot;DejaVu Sans&quot;, &quot;Bitstream Vera Sans&quot;, &quot;Trebuchet MS&quot;, Verdana, &quot;Verdana Ref&quot;, sans-serif' => 'Trebuchet',
    'Impact, Haettenschweiler, &quot;Franklin Gothic Bold&quot;, Charcoal, &quot;Helvetica Inserat&quot;, &quot;Bitstream Vera Sans Bold&quot;, &quot;Arial Black&quot;, sans-serif;' => 'Impact',
    'Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif' => 'Palatino',
    'Tahoma, Verdana, Segoe, sans-serif' => 'Tahoma'
  ) );
}
}

// Theme Customization Menu Settings
if( !function_exists( 'ts_framework_customization_tabs' ) ) {
function ts_framework_customization_tabs( $settings = array() ) {

  $settings = array(
    
    array(
      'id' => 'frone_logo',
      'options' => array(
        'title' => __('Logo', 'frone')
      ),
      'controls' => array(
        array(
          'id'      => 'frone_logo[white]',
          'label'   => __('Logo (White)', 'frone'),
          'type'    => 'image'
        ),
        array(
          'id'      => 'frone_logo[white2x]',
          'label'   => __('Logo (White)', 'frone'),
          'description' => __('For retina display @2x', 'frone'),
          'type'    => 'image'
        ),
        array(
          'id'      => 'frone_logo[colored]',
          'label'   => __('Logo (Colored)', 'frone'),
          'type'    => 'image'
        ),
        array(
          'id'      => 'frone_logo[colored2x]',
          'label'   => __('Logo (Colored)', 'frone'),
          'description' => __('For retina display @2x', 'frone'),
          'type'    => 'image'
        ),
        array(
          'id'      => 'frone_logo[maxheight]',
          'label'   => __('Logo Max Height', 'frone'),
          'default' => '44px',
          'type'    => 'text'
        ),
        array(
          'id'      => 'frone_logo[default]',
          'label'   => __('Use colored logo by default', 'frone'),
          'default' => false,
          'type'    => 'switch'
        )
      )
    ),

    array(
      'id' => 'frone_blog',
      'options' => array(
        'title' => __('Blog', 'frone')
      ),
      'controls' => array(
        array(
          'id'      => 'frone_blog[title]',
          'label'   => __('Blog', 'frone'),
          'default' => __('Blog', 'frone'),
          'type'    => 'text'
        ),
        array(
          'id'      => 'frone_blog[sidebar]',
          'label'   => __('Enable Sidebar', 'frone'),
          'type'    => 'switch'
        )
      )
    ),

    array(
      'id' => 'frone_typography',
      'options' => array(
        'title' => __('Typography', 'frone')
      ),
      'controls' => array(
        array(
          'id'      => 'frone_typography[fontpair]',
          'label'   => __('Font Pair', 'frone'),
          'default' => 'Roboto Slab, serif|Roboto, sans-serif',
          'type'    => 'chosen',
          'choices' => ts_framework_font_pair()
        ),
        array(
          'id'      => 'frone_typography[heading]',
          'label'   => __('Headings Font Family', 'frone'),
          'default' => '',
          'type'    => 'chosen',
          'choices' => ts_framework_google_fonts()
        ),
        array(
          'id'      => 'frone_typography[headingweight]',
          'label'   => __('Headings Font Weight', 'frone'),
          'default' => '700',
          'type'    => 'chosen',
          'choices' => ts_framework_fonts_weight()
        ),
        array(
          'id'      => 'frone_typography[body]',
          'label'   => __('Body Font Family', 'frone'),
          'default' => '',
          'type'    => 'chosen',
          'choices' => ts_framework_google_fonts()
        ),
        array(
          'id'      => 'frone_typography[bodyweight]',
          'label'   => __('Body Font Weight', 'frone'),
          'default' => '400',
          'type'    => 'chosen',
          'choices' => ts_framework_fonts_weight()
        ),
        /*array(
          'id'      => 'frone_typography[menu]',
          'label'   => __('Menu Font Family', 'frone'),
          'default' => '',
          'type'    => 'chosen',
          'choices' => ts_framework_google_fonts()
        ),
        array(
          'id'      => 'frone_typography[menuweight]',
          'label'   => __('Menu Font Weight', 'frone'),
          'default' => '700',
          'type'    => 'chosen',
          'choices' => ts_framework_fonts_weight()
        ),*/
        array(
          'id'      => 'frone_typography[googlesubsets]',
          'label'   => __('Google Fonts Subsets', 'frone'),
          'default' => 'latin,latin-ext',
          'type'    => 'text'
        ),
        array(
          'id'      => 'frone_typography[usecdn]',
          'label'   => __('Use CDN for Icon Packages', 'frone'),
          'type'    => 'switch'
        )
      )
    ),

    array(
      'id' => 'frone_color',
      'options' => array(
        'title' => __('Colors', 'frone')
      ),
      'controls' => array(
        array(
          'id'      => 'frone_color[accent]',
          'label'   => __('Accent Color', 'frone'),
          'default' => '#482bd5',
          'type'    => 'color'
        )
      )
    )

  );

  return $settings;
}
}

// Theme Settings
if( !function_exists( 'ts_framework_tabs' ) ) {
function ts_framework_tabs( $settings = array() ) {

  $settings = array(
    array(
      'label'   => __('Custom Sidebars', 'frone'),
      'tabs'    => array(
        array(
          'id'          => 'frone_sidebars_tabs',
          'label'       => __('Custom Sidebars', 'frone'),
          'controls'    => array(
            array(
              'id'        => 'frone_sidebars',
              'label'     => __('Sidebars', 'frone'),
              'type'      => 'group',
              'controls'  => array(
                array(
                  'id'    => 'sidebar_title',
                  'label' => __('Sidebar Title', 'frone'),
                  'type'  => 'text'
                )
              )
            )
          )
        )
      )
    )

  );

  return $settings;
}
}

// Change Framework Default Option Name
if( !function_exists( 'ts_framework_theme_option_name' ) ){
function ts_framework_theme_option_name( $option_name ) {
  return 'frone_theme_settings';
}
}

// Change Framework Logo
if( !function_exists( 'ts_framework_logo' ) ){
function ts_framework_logo( $logo ) {
  return '<img src="'.esc_url( FRONE_IMG.'/theme-settings-logo.png' ).'" alt="logo">';
}
}

//TS Framework Filters
add_filter( 'ts_framework_customization_tabs', 'ts_framework_customization_tabs' );
add_filter( 'ts_framework_tabs', 'ts_framework_tabs' );

//
add_filter( 'ts_framework_theme_option_name', 'ts_framework_theme_option_name' );
add_filter( 'ts_framework_logo', 'ts_framework_logo' );