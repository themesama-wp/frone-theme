<?php

// get required class
require_once dirname( __FILE__ ) . '/class-ts-walker-nav-menu-edit.php';

/**
 * Mega Menu
 *
 */
class TS_Mega_Menu {

  /**
   * Constructor
   *
   */
  function __construct() {
    // add custom menu fields to menu
    add_filter( 'wp_setup_nav_menu_item', array( $this, 'wp_setup_nav_menu_item' ) );
    
    // save menu custom fields
    add_action( 'wp_update_nav_menu_item', array( $this, 'wp_update_nav_menu_item' ), 10, 3 );
    
    // edit menu walker
    add_filter( 'wp_edit_nav_menu_walker', array( $this, 'wp_edit_nav_menu_walker' ), 10, 2 );

    // 
    add_filter( 'nav_menu_css_class', array( $this, 'nav_menu_css_class' ), 10, 4 );
    add_filter( 'nav_menu_item_title', array( $this, 'nav_menu_icon' ), 10, 4 );

    //custom section
    add_filter( 'nav_menu_link_attributes', array( $this, 'nav_menu_link_attributes' ), 10, 4 );
  }

  /**
   * Return Mega Menu Post Meta Values
   *
   */
  function wp_setup_nav_menu_item( $menu_item ) {
    //
    $menu_id = intval( $menu_item->ID );
    $menu_item->megamenu = get_post_meta( $menu_id, '_menu_item_megamenu', true );

    return $menu_item;
  }

  /**
   * Update Mega Menu Post Meta Values
   *
   */
  function wp_update_nav_menu_item( $menu_id, $menu_item_db_id, $args ) {
    //
    $val = isset( $_REQUEST['menu-item-megamenu'][$menu_item_db_id] ) ? $_REQUEST['menu-item-megamenu'][$menu_item_db_id] : '';
    update_post_meta( $menu_item_db_id, '_menu_item_megamenu', $val );
  }

  /**
   * Edit Nav Menu Walker
   *
   */
  function wp_edit_nav_menu_walker( $walker, $menu_id ) {
    $all_menus_locations = get_nav_menu_locations();

    if( !empty( $all_menus_locations ) && is_array( $all_menus_locations ) ) {
      foreach ($all_menus_locations as $key => $menu) {
        if( $key == 'main' && $menu_id == $menu ) {
          add_action( 'ts_walter_nav_menu_custom_section', array( $this, 'ts_walter_nav_menu_custom_section' ), 10, 2 );
        } else if( $key == 'mobile' && $menu_id == $menu ) {
          add_action( 'ts_walter_nav_menu_custom_section', array( $this, 'ts_walter_nav_menu_custom_section' ), 10, 3 );
        }

        if( ( $key == 'main' || $key == 'mobile' ) && $menu_id == $menu ) {
          $walker = 'TS_Walker_Nav_Menu_Edit';
          break;
        }
      }
    }

    return $walker;
  }

  /**
   * Custom CSS forech menu item
   *
   */
  function nav_menu_css_class( $classes, $item, $args, $depth ) {
    //
    $megamenu = !empty( $item->megamenu ) ? $item->megamenu : '';

    if( $depth == 0 ) {
      // check mega menu
      $menusize = is_array( $megamenu ) && !empty( $megamenu['menusize'] ) ? $megamenu['menusize'] : '';

      if( !empty( $menusize ) ) {
        $classes[] = 'frone-mega-menu';
        $classes[] = 'mega-col-'.str_replace('f', '', $menusize);
        if( strlen($menusize) > 1 ) {
          $classes[] = 'mega-col-full';
        }
      }

    } else {
      //
    }

    // check seperate
    if( is_array( $megamenu ) && !empty( $megamenu['seperator'] ) ) {
      $classes[] = 'seperate-after-menu-item';
    }

    return $classes;
  }

  /**
   * Change Link Attributes
   *
   */
  function nav_menu_link_attributes( $atts, $item, $args, $depth ) {
    //
    $disablelink = !empty( $item->megamenu ) && !empty( $item->megamenu['disablelink'] ) ? true : false;

    if( $disablelink && !empty( $atts['href'] ) ) {
      $atts['href'] = '#';
    }

    return $atts;
  }

  /**
   * Menu Icon
   *
   */
  function nav_menu_icon( $title, $item, $args, $depth ) {
    //
    $fonticon = !empty( $item->megamenu ) && !empty( $item->megamenu['fonticon'] ) ? $item->megamenu['fonticon'] : '';
    
    //check font type
    if( substr( $fonticon, 0, 3 ) === "fa-" ) {
      $fonticon_container = '<i class="frone-menu-icon fa '.esc_attr( $fonticon ).'" aria-hidden="true"></i>';
    } else if( !empty( $fonticon ) ) {
      $fonticon_container = '<i class="frone-menu-icon md-icon">'.esc_html( $fonticon ).'</i>';
    }

    if( !empty( $fonticon_container ) ) {
      $title = $fonticon_container.$title;
    }

    return $title;
  }

  /**
   * Custom Sections
   *
   */
  function ts_walter_nav_menu_custom_section( $item, $depth, $mobile = false ) {
    //
    $item_id = esc_attr( $item->ID );
    $megamenu = $item->megamenu;

    echo '<div class="ts-extra-menu-settings description description-wide '.($mobile ? 'ts-mega-menu-mobile' : '').'">';

    //Mega Menu Option
    if( !$mobile ) {
      $menusize = is_array( $megamenu ) && !empty( $megamenu['menusize'] ) ? $megamenu['menusize'] : '';

      echo '<p class="field-custom field-mega-menu-size description description-wide" style="display: none;">
      <label>
        '.__( 'Mega Menu', 'frone' ).'<br />
        <select name="menu-item-megamenu['.$item_id.'][menusize]" class="ts-full-width-select">
          <option value="">'.esc_html__('Disabled', 'frone').'</option>
          <optgroup label="'.esc_attr__('Mega menu', 'frone').'">
            <option value="1" '.selected($menusize, '1', false).'>'.esc_html__('Column Free', 'frone').'</option>
            <option value="2" '.selected($menusize, '2', false).'>'.esc_html__('2 Columns', 'frone').'</option>
            <option value="3" '.selected($menusize, '3', false).'>'.esc_html__('3 Columns', 'frone').'</option>
            <option value="4" '.selected($menusize, '4', false).'>'.esc_html__('4 Columns', 'frone').'</option>
          </optgroup>
          <optgroup label="'.esc_attr__('Full Width Mega menu', 'frone').'">
            <option value="1f" '.selected($menusize, '1f', false).'>'.esc_html__('Column Free', 'frone').'</option>
            <option value="2f" '.selected($menusize, '2f', false).'>'.esc_html__('2 Columns', 'frone').'</option>
            <option value="3f" '.selected($menusize, '3f', false).'>'.esc_html__('3 Columns', 'frone').'</option>
            <option value="4f" '.selected($menusize, '4f', false).'>'.esc_html__('4 Columns', 'frone').'</option>
          </optgroup>
        </select>
      </label></p>';
    }

    /**
     * General Settings
     *
     */

    //icon
    $fonticon = is_array( $megamenu ) && !empty( $megamenu['fonticon'] ) ? $megamenu['fonticon'] : '';

    echo '<div class="field-custom description description-wide"><label>'.__('Icon', 'frone').'</label><br>
      <input id="menu-item-megamenu-icon-switcher-'.$item_id.'" name="menu-item-megamenu['.$item_id.'][fonticon]" value="'.esc_attr( $fonticon ).'" type="text">
    </div>';

    //seperator
    $seperator = is_array( $megamenu ) && !empty( $megamenu['seperator'] ) ? 'on' : '';

    echo '<div class="field-custom field-mega-menu-seperator description description-wide"><label>'.__('Seperator After Menu Item', 'frone').'</label>
    <div class="ts-switch">
      <input '.checked( 'on', $seperator, false ).' id="menu-item-megamenu-switch-seperator-'.$item_id.'" name="menu-item-megamenu['.$item_id.'][seperator]" type="checkbox">
      <label for="menu-item-megamenu-switch-seperator-'.$item_id.'"></label>
    </div></div>';

    //disable link
    $disablelink = is_array( $megamenu ) && !empty( $megamenu['disablelink'] ) ? 'on' : '';

    echo '<div class="field-custom field-mega-menu-disable-link description description-wide"><label>'.__('Disable Menu Link', 'frone').'</label>
    <div class="ts-switch">
      <input '.checked( 'on', $disablelink, false ).' id="menu-item-megamenu-switch-disable-menu-link-'.$item_id.'" name="menu-item-megamenu['.$item_id.'][disablelink]" type="checkbox">
      <label for="menu-item-megamenu-switch-disable-menu-link-'.$item_id.'"></label>
    </div></div>';

    echo '</div>';
  }

}
new TS_Mega_Menu;

class TS_Mega_Menu_Frone extends Walker_Nav_Menu {
  public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;

    /**
     * Filters the arguments for a single nav menu item.
     *
     * @since 4.4.0
     *
     * @param stdClass $args  An object of wp_nav_menu() arguments.
     * @param WP_Post  $item  Menu item data object.
     * @param int      $depth Depth of menu item. Used for padding.
     */
    $args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

    /**
     * Filters the CSS class(es) applied to a menu item's list item element.
     *
     * @since 3.0.0
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param array    $classes The CSS classes that are applied to the menu item's `<li>` element.
     * @param WP_Post  $item    The current menu item.
     * @param stdClass $args    An object of wp_nav_menu() arguments.
     * @param int      $depth   Depth of menu item. Used for padding.
     */
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    /**
     * Filters the ID applied to a menu item's list item element.
     *
     * @since 3.0.1
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
     * @param WP_Post  $item    The current menu item.
     * @param stdClass $args    An object of wp_nav_menu() arguments.
     * @param int      $depth   Depth of menu item. Used for padding.
     */
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $class_names .'>';

    $atts = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
    $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
    $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

    /**
     * Filters the HTML attributes applied to a menu item's anchor element.
     *
     * @since 3.6.0
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param array $atts {
     *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
     *
     *     @type string $title  Title attribute.
     *     @type string $target Target attribute.
     *     @type string $rel    The rel attribute.
     *     @type string $href   The href attribute.
     * }
     * @param WP_Post  $item  The current menu item.
     * @param stdClass $args  An object of wp_nav_menu() arguments.
     * @param int      $depth Depth of menu item. Used for padding.
     */
    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( ! empty( $value ) ) {
        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    /** This filter is documented in wp-includes/post-template.php */
    $title = apply_filters( 'the_title', $item->title, $item->ID );

    /**
     * Filters a menu item's title.
     *
     * @since 4.4.0
     *
     * @param string   $title The menu item's title.
     * @param WP_Post  $item  The current menu item.
     * @param stdClass $args  An object of wp_nav_menu() arguments.
     * @param int      $depth Depth of menu item. Used for padding.
     */
    $title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . $title . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    /**
     * Filters a menu item's starting output.
     *
     * The menu item's starting output only includes `$args->before`, the opening `<a>`,
     * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
     * no filter for modifying the opening and closing `<li>` for a menu item.
     *
     * @since 3.0.0
     *
     * @param string   $item_output The menu item's starting HTML output.
     * @param WP_Post  $item        Menu item data object.
     * @param int      $depth       Depth of menu item. Used for padding.
     * @param stdClass $args        An object of wp_nav_menu() arguments.
     */
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}