<?php
/**
 * Theme WooCommerce Settings
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}