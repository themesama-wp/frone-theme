<?php
/**
 * Theme Functions
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Theme Support, Nav & Textdomain
 * 
 * @since 1.0
 */
if( !function_exists( 'frone_theme_support' ) ) {
function frone_theme_support() {
  //Site Title Tag
  add_theme_support( 'title-tag' );

  //Post Formats
  add_theme_support( 'post-formats', array(
    'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'
  ));

  //Post Thumbnails
  add_theme_support( 'post-thumbnails', array(
    'post', 'page'
  ));

  //Feed Links
  add_theme_support( 'automatic-feed-links' );

  //HTML5
  add_theme_support( 'html5', array(
    'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'
  ));

  //WooCommerce
  add_theme_support( 'woocommerce' );

  //Register Nav Menus
  register_nav_menus( array(
    'main'    => __('Main Menu', 'frone' ),
    'mobile'  => __('Mobile Menu', 'frone' )
  ));

  //Load Theme Textdomain
  load_theme_textdomain( 'frone', FRONE_PATH.'/languages' );
 
}
}

/**
 * Widgets init
 * 
 * @since 1.0
 */
if( !function_exists( 'frone_widgets_init' ) ) {
function frone_widgets_init() {
  // Widget Defaults
  $widget_defaults = apply_filters( 'frone_widget_defaults', array(
    'before_widget' => '<section id="%1$s" class="frone-widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h4 class="widget-title">',
    'after_title'   => '</h4>'
  ) );


  // Main Sidebar for Blog
  register_sidebar( wp_parse_args( array(
    'name'          => __( 'Main Sidebar', 'frone' ),
    'id'            => 'frone-sidebar-main',
    'description'   => __( 'Add widgets here to appear in your sidebar.', 'frone' )
  ), $widget_defaults) );

  // WooCommerce Sidebar
  if( is_woocommerce_activated() ) {
    register_sidebar( wp_parse_args( array(
      'name'          => __( 'Shop Sidebar', 'frone' ). ' - WooCommerce',
      'id'            => 'frone-sidebar-shop',
      'description'   => sprintf(__( 'Add widgets here to appear in your "%s" sidebar.', 'frone' ), 'WooCommerce')
    ), $widget_defaults) );
  }

  // Easy Digital Downloads Sidebar
  if( is_edd_activated() ) {
    register_sidebar( wp_parse_args( array(
      'name'          => __( 'Shop Sidebar', 'frone' ). ' - Easy Digital Downloads',
      'id'            => 'frone-sidebar-shop-edd',
      'description'   => sprintf(__( 'Add widgets here to appear in your "%s" sidebar.', 'frone' ), 'Easy Digital Downloads')
    ), $widget_defaults) );
  }

  // Custom Sidebars
  $custom_sidebars = ts_get_option( 'frone_sidebars' );

  if( !empty( $custom_sidebars ) && is_array($custom_sidebars) ) {
    foreach ($custom_sidebars as $sidebar_id => $sidebar) {
      //
      if( !empty($sidebar['sidebar_title']) ) {
        // create id
        $sidebar_id = sanitize_title( $sidebar['sidebar_title'] );

        //register custom sidebars
        register_sidebar( wp_parse_args( array(
          'name'          => $sidebar['sidebar_title'],
          'id'            => $sidebar_id,
          'description'   => sprintf(__( 'Add widgets here to appear in your "%s" sidebar.', 'frone' ), $sidebar['sidebar_title'])
        ), $widget_defaults) );
      }
    }
  }
  
}
}

/**
 * Frone Theme Loop Posts
 *
 * @since 1.0
 */
if( !function_exists( 'frone_loop' ) ) {
function frone_loop() {
  get_template_part( 'template-parts/post/content', 'loop' );
}
}

/**
 * Theme Taxonomy Post Format Title
 * 
 * @since 1.0
 */
if( !function_exists( 'frone_taxonomy_post_format_title' ) ) {
function frone_taxonomy_post_format_title() {
  echo '<h1>';

  if ( is_tax( 'post_format', 'post-format-aside' ) ) {
    _e( 'Asides', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-image' ) ) {
    _e( 'Images', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-video' ) ) {
    _e( 'Videos', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-audio' ) ) {
    _e( 'Audio', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-quote' ) ) {
    _e( 'Quotes', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-link' ) ) {
    _e( 'Links', 'frone' );
  }else if ( is_tax( 'post_format', 'post-format-gallery' ) ) {
    _e( 'Galleries', 'frone' );
  }else {
    _e( 'Archives', 'frone' );
  }

  echo '</h1>';
}
}

/**
 * Theme Logo
 *
 */
if( !function_exists( 'frone_logo' ) ) {
function frone_logo() {
  $frone_logo = frone_theme_mod( 'logo' );
  $logo_alt = get_bloginfo( 'name' );
  $home_page_url = get_home_url();

  $logo_container = '<div id="logo">';

  // generate logo type
  if( !empty($frone_logo) && (!empty( $frone_logo['white'] ) || !empty( $frone_logo['white2x'] ) || !empty( $frone_logo['colored'] ) || !empty( $frone_logo['colored2x'] )) ) {
    $logo_container.= '<a href="'.esc_url( $home_page_url ).'">';
    
    //white logo
    $logo_container.= !empty( $frone_logo['white'] ) ? '<img src="'.esc_attr( $frone_logo['white'] ).'" class="white-logo" alt="'.esc_attr( $logo_alt ).'">' : '';
    //white2x logo
    $logo_container.= !empty( $frone_logo['white2x'] ) ? '<img src="'.esc_attr( $frone_logo['white2x'] ).'" class="white2x-logo" alt="'.esc_attr( $logo_alt ).'">' : '';
    //colored logo
    $logo_container.= !empty( $frone_logo['colored'] ) ? '<img src="'.esc_attr( $frone_logo['colored'] ).'" class="colored-logo" alt="'.esc_attr( $logo_alt ).'">' : '';
    //colored2x logo
    $logo_container.= !empty( $frone_logo['colored2x'] ) ? '<img src="'.esc_attr( $frone_logo['colored2x'] ).'" class="colored2x-logo" alt="'.esc_attr( $logo_alt ).'">' : '';

    $logo_container.= '</a>';

  } else {
    $logo_container.= '<h1><a href="'.esc_url( $home_page_url ).'"><span class="md-icon">whatshot</span>'.esc_html( $logo_alt ).'</h1>';
  }

  $logo_container.= '</div>';

  echo $logo_container;
}
}

/**
 * Theme Main Menu
 *
 */
if( !function_exists( 'frone_main_menu' ) ) {
function frone_main_menu() {
  $wp_menu_args = array(
    'menu_id'         => 'frone-main-menu',
    'menu_class'      => 'frone-nav',
    'fallback_cb'     => false,
    'container'       => 'nav',
    'container_class' => 'frone-nav-container',
    'theme_location'  => 'main'
  );

  if ( has_nav_menu( 'main' ) ) {
    wp_nav_menu( $wp_menu_args );
  }
}
}

/**
 * Theme Mobile Menu
 *
 */
if( !function_exists( 'frone_mobile_menu' ) ) {
function frone_mobile_menu() {
  $wp_menu_args = array(
    'menu_id'         => 'frone-mobile-menu',
    'menu_class'      => 'frone-mobile-nav',
    'fallback_cb'     => false,
    'container'       => false,
    'theme_location'  => 'mobile'
  );

  if ( has_nav_menu( 'mobile' ) ) {
    echo '<div class="frone-mobile-sidemenu">
      <a href="#" class="frone-mobile-menu-btn md-icon">menu</a>
      <nav class="frone-mobile-nav-container">';

        wp_nav_menu( $wp_menu_args );

    echo '</nav><div class="frone-mobile-nav-bg"></div></div>';

  }
}
}

/**
 * Theme Extra Menu
 *
 */
if( !function_exists( 'frone_extra_menu' ) ) {
function frone_extra_menu() {
  echo '<div class="frone-extra-nav-container">
    <ul class="frone-extra-nav">';

  if( is_woocommerce_activated() ) {
    echo '<li class="frone-extra-menu-item"><a href="#"><i class="md-icon">account_circle</i></a></li>';
    echo '<li class="frone-extra-menu-item"><a href="#"><i class="md-icon">shopping_basket</i><span class="badge">4</span></a></li>';
  }

  echo '<li class="frone-extra-menu-item"><a href="#"><i class="md-icon">favorite</i></a></li>';
  echo '<li class="frone-extra-menu-item"><a href="#"><i class="md-icon">search</i></a></li>';

  echo '</ul></div>';
}
}