<?php
/**
 * Register/Enqueue Styles & Scripts
 * 
 * @since 1.0
 */
if( !function_exists( 'frone_enqueue_scripts' ) ) {
function frone_enqueue_scripts() {
  //Enqueue styles
  wp_enqueue_style( 'google-fonts-frone', frone_fonts_url(), array(), null );
  wp_enqueue_style( 'main-frone', FRONE_CSS.'/frone'.FRONE_MIN.'.css', array(), FRONE_VERSION );
  
  //Theme Font Icons
  $frone_icons = frone_theme_mod( 'typography', 'usecdn' ) ? 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' : FRONE_CSS.'/icons'.FRONE_MIN.'.css';
  wp_enqueue_style( 'icons-frone', $frone_icons, array('main-frone'), FRONE_VERSION );
  frone_remove_duplicated_icon_fonts();

  //RTL
  if( is_rtl() ) {
    wp_enqueue_style( 'rtl-frone', FRONE_CSS.'/rtl'.FRONE_MIN.'.css', array(), FRONE_VERSION );
  }

  //Enqueue scripts
  wp_enqueue_script( 'plugins-frone', FRONE_JS.'/jquery.plugins'.FRONE_MIN.'.js', array('jquery'), FRONE_VERSION, true );
  wp_enqueue_script( 'register-frone', FRONE_JS.'/jquery.frone'.FRONE_MIN.'.js', array('plugins-frone'), FRONE_VERSION, true );
  
  //comment-reply
  if ( is_singular() ) {
    wp_enqueue_script( 'comment-reply' );
  }
  
  //Load original resources
  if( !FRONE_MIN ) {
    
  }

}

/**
 * Registers an editor stylesheet for the theme.
 *
 * @since 1.0
 */
if( !function_exists( 'frone_add_editor_styles' ) ) {
function frone_add_editor_styles() {
  add_editor_style( array('css/editor'.FRONE_MIN.'.css', frone_fonts_url()) );
}
}
}

/**
 * Body Class
 *
 * @since 1.0
 */
if( !function_exists( 'frone_body_class' ) ) {
function frone_body_class( $classes ) {
  // default logo
  if( frone_theme_mod( 'logo', 'default' ) && (!empty( frone_theme_mod( 'logo', 'colored' ) ) || !empty( frone_theme_mod( 'logo', 'colored2x' ) ) ) ) {
    $classes[] = 'default-colored-logo';
  }

  //retina logo
  if( !empty( frone_theme_mod( 'logo', 'colored2x' ) ) ) {
    $classes[] = 'retina-colored-logo';
  }

  if( !empty( frone_theme_mod( 'logo', 'white2x' ) ) ) {
    $classes[] = 'retina-white-logo';
  }

  return $classes;
}
}

/**
 * Frone Custom CSS
 *
 * @since 1.0
 */
if( !function_exists( 'frone_custom_inline_styles' ) ) {
function frone_custom_inline_styles() {
  //get typography
  $frone_typography = frone_theme_mod( 'typography' );
  $frone_font_pair = !empty( $frone_typography['fontpair'] ) && strpos( $frone_typography['fontpair'], '|' ) ? explode( '|', $frone_typography['fontpair'] ) : array('inherit', 'inherit');
  
  //heading
  $frone_heading_font_family = !empty( $frone_typography['heading'] ) ? $frone_typography['heading'] : $frone_font_pair[0];
  $frone_heading_font_family = substr_count($frone_heading_font_family, ',') == 1 ? '"'.str_replace(',', '",', $frone_heading_font_family) : $frone_heading_font_family;
  $frone_heading_font_weight = !empty( $frone_typography['headingweight'] ) ? $frone_typography['headingweight'] : '700';

  //body
  $frone_body_font_family = !empty( $frone_typography['body'] ) ? $frone_typography['body'] : $frone_font_pair[1];
  $frone_body_font_family = substr_count($frone_body_font_family, ',') == 1 ? '"'.str_replace(',', '",', $frone_body_font_family) : $frone_body_font_family;
  $frone_body_font_weight = !empty( $frone_typography['bodyweight'] ) ? $frone_typography['bodyweight'] : '400';

  //
  $custom_css = '';

  // Font Family
  $custom_css.= 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6{font-family: '.$frone_heading_font_family.'; font-weight: '.$frone_heading_font_weight.';}';
  $custom_css.= 'body{font-family: '.$frone_body_font_family.'; font-weight: '.$frone_body_font_weight.';}';

  // Logo Max Height
  $custom_css.= !empty( frone_theme_mod( 'logo', 'maxheight' ) ) ? '#logo img {max-height:'.frone_theme_mod( 'logo', 'maxheight' ).';}' : '';

  // Text Logo Color
  $custom_css.= !empty( frone_theme_mod( 'color', 'accent' ) ) ? '#logo h1 a{color:'.frone_theme_mod( 'color', 'accent' ).';}' : '';

  wp_add_inline_style( 'main-frone', $custom_css );
}
}