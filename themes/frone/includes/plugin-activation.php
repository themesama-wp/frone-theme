<?php
/**
 * Theme Compatible Plugin List
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

// get required class
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

if( !function_exists('frone_register_required_plugins') ) {
function frone_register_required_plugins() {

  $activate_plugins = array();

  // Suggested Plugins
  $theme_plugins = array(
    array(
      'name'      => 'Visual Composer',
      'slug'      => 'js_composer',
      'source'    => get_stylesheet_directory() . '/plugins/js_composer.zip',
      'required'  => true,
      'version'   => ''
    ),
    array(
      'name'      => 'Slider Revolution',
      'slug'      => 'revslider',
      'source'    => get_stylesheet_directory() . '/plugins/revslider.zip',
    ),
    array(
      'name'      => 'WooCommerce',
      'slug'      => 'woocommerce',
      'required'  => true
    ),
    array(
      'name'      => 'Yith WooCommerce Wishlist',
      'slug'      => 'yith-woocommerce-wishlist'
    ),
    array(
      'name'      => 'Yith WooCommerce Compare',
      'slug'      => 'yith-woocommerce-compare'
    ),
    array(
      'name'      => 'Yith WooCommerce Quick View',
      'slug'      => 'yith-woocommerce-quick-view'
    ),
    array(
      'name'      => 'WooCommerce Currency Switcher',
      'slug'      => 'woocommerce-currency-switcher'
    ),
    array(
      'name'      => 'Stripe for WooCommerce',
      'slug'      => 'stripe-for-woocommerce'
    ),
    array(
      'name'      => 'Easy Digital Downloads',
      'slug'      => 'easy-digital-downloads'
    ),
    array(
      'name'      => 'Contact Form 7',
      'slug'      => 'contact-form-7'
    ),
    array(
      'name'      => 'BuddyPress',
      'slug'      => 'buddypress'
    ),
    array(
      'name'      => 'bbPress',
      'slug'      => 'bbpress'
    )
  );

  // Development Plugins
  $dev_plugins = array(
    array(
      'name'              => 'Theme Check',
      'slug'              => 'theme-check',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'Monster Widget',
      'slug'              => 'monster-widget',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'WooCommerce Monster Widget',
      'slug'              => 'woocommerce-monster-widget',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'EDD Monster Widget',
      'slug'              => 'edd-monster-widget',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'Ninja Forms',
      'slug'              => 'ninja-forms',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'WordPress Importer',
      'slug'              => 'wordpress-importer',
      'required'          => true,
      'force_activation'  => true
    ),
    array(
      'name'              => 'RTL Tester',
      'slug'              => 'rtl-tester',
      'required'          => true,
      'force_activation'  => true
    )
  );

  // Check development mode
  if( FRONE_MODE == 'dev' ) {
    $activate_plugins = array_merge($theme_plugins, $dev_plugins);
  }

  /**
   * Array of configuration settings. Amend each line as needed.
   * If you want the default strings to be available under your own theme domain,
   * leave the strings uncommented.
   * Some of the strings are added into a sprintf, so see the comments at the
   * end of each line for what each argument will be.
   */
  $config = array(
    'default_path'                      => '',                      // Default absolute path to pre-packaged plugins.
    'menu'                              => 'tgmpa-install-plugins', // Menu slug.
    'has_notices'                       => true,                    // Show admin notices or not.
    'dismissable'                       => true,                    // If false, a user cannot dismiss the nag message.
    'dismiss_msg'                       => '',                      // If 'dismissable' is false, this message will be output at top of nag.
    'is_automatic'                      => false,                   // Automatically activate plugins after installation or not.
    'message'                           => '',                      // Message to output right before the plugins table.
    'strings'                           => array(
      'page_title'                      => __( 'Install Required Plugins', 'frone' ),
      'menu_title'                      => __( 'Install Plugins', 'frone' ),
      'installing'                      => __( 'Installing Plugin: %s', 'frone' ), // %s = plugin name.
      'oops'                            => __( 'Something went wrong with the plugin API.', 'frone' ),
      'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'frone' ), // %1$s = plugin name(s).
      'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'frone' ), // %1$s = plugin name(s).
      'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'frone' ), // %1$s = plugin name(s).
      'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'frone' ), // %1$s = plugin name(s).
      'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'frone' ), // %1$s = plugin name(s).
      'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'frone' ), // %1$s = plugin name(s).
      'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'frone' ), // %1$s = plugin name(s).
      'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'frone' ), // %1$s = plugin name(s).
      'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'frone' ),
      'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'frone' ),
      'return'                          => __( 'Return to Required Plugins Installer', 'frone' ),
      'plugin_activated'                => __( 'Plugin activated successfully.', 'frone' ),
      'complete'                        => __( 'All plugins installed and activated successfully. %s', 'frone' ), // %s = dashboard link.
      'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
    )
  );

  if( function_exists( 'tgmpa' ) ) {
    tgmpa( ( !empty($activate_plugins) ? $activate_plugins : $theme_plugins ) , $config );
  }
 
}
}