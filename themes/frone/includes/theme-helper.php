<?php
/**
 * Theme Helper
 *
 * @author Themesama
 * @since 1.0
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}

/**
 * Get Theme Mod
 * 
 * @since 1.0
 */
if( !function_exists( 'frone_theme_mod' ) ) {
function frone_theme_mod($setting_id = 'blog', $value = '') {
  //
  $frone_options = get_theme_mod( 'frone_'.$setting_id );

  if( empty($value) && !empty($frone_options) ) {
    return $frone_options;
  }else if ( !empty( $frone_options[$value] ) ) {
    return $frone_options[$value];
  }else{
    return '';
  }
}
}

/**
 * Check WooCommerce Plugin Status
 * 
 * @since 1.0
 */
if( !function_exists( 'is_woocommerce_activated' ) ) {
function is_woocommerce_activated() {
  if( ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'woocommerce' ) ) || class_exists( 'woocommerce' ) ) {
    return true;
  }

  return false;
}
}

/**
 * Check Easy Digital Downloads Plugin Status
 * 
 * @since 1.0
 */
if ( !function_exists( 'is_edd_activated' ) ) {
function is_edd_activated() {
  if( ( function_exists( 'is_plugin_active' ) && is_plugin_active( 'easy-digital-downloads' ) ) || class_exists( 'Easy_Digital_Downloads' ) ) {
    return true;
  }

  return false;
}
}

/**
 * Register custom fonts
 *
 * @since 1.0
 */
if( !function_exists( 'frone_fonts_url' ) ) {
function frone_fonts_url() {
  //
  $font_families = apply_filters('frone_google_font_families', array());

  //get typography
  $frone_typography = frone_theme_mod( 'typography' );
  $frone_font_pair = !empty( $frone_typography['fontpair'] ) && strpos( $frone_typography['fontpair'], '|' ) ? explode( '|', $frone_typography['fontpair'] ) : array('inherit', 'inherit');
  
  //heading
  $frone_heading_font_family = !empty( $frone_typography['heading'] ) ? $frone_typography['heading'] : $frone_font_pair[0];
  $frone_heading_font_family = substr_count($frone_heading_font_family, ',') == 1 ? strtok($frone_heading_font_family, ',') : '';
  if( $frone_heading_font_family ) {
    $font_families[] = $frone_heading_font_family.( !empty( $frone_typography['headingweight'] ) ? ':'.$frone_typography['headingweight'] : '' );
  }
  
  //body
  $frone_body_font_family = !empty( $frone_typography['body'] ) ? $frone_typography['body'] : $frone_font_pair[1];
  $frone_body_font_family = substr_count($frone_body_font_family, ',') == 1 ? strtok($frone_body_font_family, ',') : '';
  if( $frone_body_font_family ) {
    $font_families[] = $frone_body_font_family.( !empty( $frone_typography['bodyweight'] ) ? ':'.$frone_typography['bodyweight'] : '' );
  }

  //icon font
  if( frone_theme_mod( 'typography', 'usecdn' ) ) {
    $font_families[] = 'Material Icons';
  }

  if( empty( $font_families ) ) {
    return false;
  }

  $query_args = array(
    'family' => urlencode( implode( '|', $font_families ) ),
    'subset' => urlencode( !empty( $frone_typography['googlesubsets'] ) ? $frone_typography['googlesubsets'] : 'latin' )
  );

  $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

  return esc_url_raw( $fonts_url );
}
}

/**
 * Remove duplicated icon fonts
 *
 * @since 1.0
 */
if( !function_exists( 'frone_remove_duplicated_icon_fonts' ) ) {
function frone_remove_duplicated_icon_fonts() {
  //
  wp_deregister_style( 'font-awesome' );
  wp_dequeue_style( 'font-awesome' );

  //
  wp_dequeue_style( 'yith-wcwl-font-awesome' );
}
}