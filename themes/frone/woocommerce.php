<?php
/**
 * The template for displaying WooCommerce products
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <?php woocommerce_content(); ?>

<?php get_footer(); ?>