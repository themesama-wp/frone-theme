<?php
/**
 * The template for displaying search results pages
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <?php if ( have_posts() ) : ?>
    <h1><?php printf( __( 'Search Results for: %s', 'frone' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
  <?php else : ?>
    <h1><?php _e( 'Nothing Found', 'frone' ); ?></h1>
  <?php endif; ?>

    <?php
    if ( have_posts() ) :
      /* Start the Loop */
      while ( have_posts() ) : the_post();

        /**
         * Run the loop for the search to output the results.
         * If you want to overload this in a child theme then include a file
         * called content-search.php and that will be used instead.
         */
        get_template_part( 'template-parts/post/content', 'excerpt' );

      endwhile; // End of the loop.

      the_posts_pagination( array(
        'prev_text' => __( 'Previous page', 'frone' ),
        'next_text' => __( 'Next page', 'frone' ),
        'before_page_number' => __( 'Page', 'frone' ),
      ) );

    else : ?>

      <p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'frone' ); ?></p>
      <?php
        get_search_form();

    endif;
    ?>

  <?php get_sidebar(); ?>

<?php get_footer(); ?>