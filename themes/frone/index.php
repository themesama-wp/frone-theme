<?php
/**
 * The main template file
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <?php if ( is_home() && ! is_front_page() ) : ?>
    <h1><?php single_post_title(); ?></h1>
  <?php else : ?>
    <h2><?php _e( 'Posts', 'frone' ); ?></h2>
  <?php endif; ?>

  <?php
    /**
     * 20 - frone_loop
     *
     */
    do_action( 'frone_index' ); ?>

<?php get_footer(); ?>
