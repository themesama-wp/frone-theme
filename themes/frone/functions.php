<?php
/**
 * Frone functions and definitions
 *
 * @author Themesama
 * @since 1.0
 */

/**
 * Define Theme Helper Variables
 *
 * @since 1.0
 */
if ( !isset( $content_width ) ) {
  $content_width = 1200;
}

$frone_theme = wp_get_theme( 'frone' );

defined( 'FRONE_VERSION' )    || define( 'FRONE_VERSION' , $frone_theme->Version );

defined( 'FRONE_PATH' )       || define( 'FRONE_PATH', get_template_directory() );
defined( 'FRONE_INC' )        || define( 'FRONE_INC',  FRONE_PATH.'/includes' );
defined( 'FRONE_FRAMEWORK' )  || define( 'FRONE_FRAMEWORK',  FRONE_PATH.'/ts-framework' );

defined( 'FRONE_URI' )        || define( 'FRONE_URI' , get_template_directory_uri() );
defined( 'FRONE_IMG' )        || define( 'FRONE_IMG' , FRONE_URI.'/images' );
defined( 'FRONE_JS' )         || define( 'FRONE_JS'  , FRONE_URI.'/js' );
defined( 'FRONE_CSS' )        || define( 'FRONE_CSS' , FRONE_URI.'/css' );

defined( 'FRONE_MODE' )       || define( 'FRONE_MODE' , 'dev' );
defined( 'FRONE_MIN' )        || define( 'FRONE_MIN', '.min' );

/**
 * Load Templates
 *
 * @since 1.0
 */

//Theme Helper & Functions
load_template( FRONE_INC.'/theme-helper.php' );
load_template( FRONE_INC.'/theme-functions.php' );
load_template( FRONE_INC.'/theme-style.php' );
load_template( FRONE_INC.'/theme-backend.php' );
load_template( FRONE_INC.'/theme-megamenu.php' );

//WooCommerce
load_template( FRONE_INC.'/theme-woocommerce.php' );

//TGM Plugin Activation
load_template( FRONE_INC.'/plugin-activation.php' );

//Theme framework
load_template( FRONE_FRAMEWORK.'/admin.php' );

//Theme Hooks
load_template( FRONE_INC.'/theme-hooks.php' );