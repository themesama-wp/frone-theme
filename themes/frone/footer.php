<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @author Themesama
 * @since 1.0
 */

?>

<?php 
  /**
   *
   */
  do_action( 'frone_footer' ); ?>

<?php wp_footer(); ?>

</body>
</html>