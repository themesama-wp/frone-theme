<?php
/**
 * The template for displaying all pages
 *
 * @author Themesama
 * @since 1.0
 */

get_header(); ?>

  <?php
  while ( have_posts() ) { 
    the_post();
    get_template_part( 'template-parts/page/content', 'page' );

    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;

  } // End of the loop.
  ?>

<?php get_footer(); ?>